package online.zdenda.dolphins.common.quiz.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import online.zdenda.dolphins.R
import online.zdenda.dolphins.common.quiz.domain.question.SimpleSingleAnswerQuestion
import online.zdenda.dolphins.databinding.QuizSingleAnswerQuestionBinding


class QuizSimpleSingleAnswerQuestionFragment : Fragment() {

    private val viewModel: QuizViewModel by activityViewModels()

    private var _binding: QuizSingleAnswerQuestionBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = QuizSingleAnswerQuestionBinding.inflate(inflater, container, false)

        addOptionsWithListeners()

        return binding.root
    }

    private fun addOptionsWithListeners() {
        val question = viewModel.currentQuestion<SimpleSingleAnswerQuestion<*>, Nothing>()
        binding.quizSingleAnswerGroup.removeAllViews()
        question.options.forEach { option ->
            val optionButton = RadioButton(context)
            optionButton.id = View.generateViewId()
            optionButton.text = option.value
            optionButton.setTextAppearance(R.style.Theme_Dolphins_RadioButton)
            optionButton.setOnClickListener {
                viewModel.changeAnswer(option.value)
            }
            binding.quizSingleAnswerGroup.addView(optionButton)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}