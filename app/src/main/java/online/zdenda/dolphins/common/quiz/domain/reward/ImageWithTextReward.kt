package online.zdenda.dolphins.common.quiz.domain.reward

data class ImageWithTextReward(
    val text: String,
    val imageResourceId: Int
) : QuizQuestionReward