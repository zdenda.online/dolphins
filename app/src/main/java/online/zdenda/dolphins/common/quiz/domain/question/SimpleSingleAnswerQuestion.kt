package online.zdenda.dolphins.common.quiz.domain.question

import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward

class SimpleSingleAnswerQuestion<R : QuizQuestionReward>(
    text: String,
    password: String,
    options: List<AnswerOption<String>>,
    reward: R,
) : SingleAnswerQuestion<String, R>(text, password, options, reward)