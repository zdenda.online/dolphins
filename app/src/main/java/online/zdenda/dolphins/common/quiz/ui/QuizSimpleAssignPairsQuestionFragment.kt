package online.zdenda.dolphins.common.quiz.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.LinearLayout.LayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import online.zdenda.dolphins.R
import online.zdenda.dolphins.common.quiz.domain.question.SimpleAssignPairsQuestion
import online.zdenda.dolphins.common.app.ui.attrValue
import online.zdenda.dolphins.databinding.LinearContainerBinding

class QuizSimpleAssignPairsQuestionFragment : Fragment() {

    private val viewModel: QuizViewModel by activityViewModels()

    private var _binding: LinearContainerBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView
    private val answerComponents = mutableMapOf<String, Spinner>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LinearContainerBinding.inflate(inflater, container, false)

        addPairsWithListeners()

        return binding.root
    }

    private fun addPairsWithListeners() {
        val question = viewModel.currentQuestion<SimpleAssignPairsQuestion<*>, Nothing>()
        val originalAnswers = question.pairs.map { it.second }
        val randomizedAnswers = question.randomizePairs().map { it.second }

        binding.linearContainer.removeAllViews()
        question.pairs.forEachIndexed { idx, pair ->
            val pairLayout = preparePairLayout()

            val textView = preparePairFirstView(pair.first)
            pairLayout.addView(textView)

            val randomFirstAnswer = randomizedAnswers[idx]
            val spinnerView = preparePairSecondView(originalAnswers, randomFirstAnswer)
            pairLayout.addView(spinnerView)

            answerComponents[pair.first] = spinnerView
            binding.linearContainer.addView(pairLayout)
        }
    }

    private fun preparePairLayout(): LinearLayout {
        val pairLayout = LinearLayout(context)
        pairLayout.orientation = LinearLayout.VERTICAL
        val layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        pairLayout.layoutParams = layoutParams
        return pairLayout
    }

    private fun preparePairFirstView(value: String): TextView {
        val topBottomMargin = resources.getDimension(R.dimen.content_grid_gap).toInt()
        val textViewParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        textViewParams.setMargins(0, topBottomMargin, 0, topBottomMargin)

        val textView = TextView(context)
        val textValue = "${value}..."
        textView.text = textValue
        textView.setTextAppearance(R.style.Theme_Dolphins_LargeText)
        textView.setTextColor(requireContext().attrValue(R.attr.colorSecondary))
        textView.layoutParams = textViewParams
        return textView
    }

    private fun preparePairSecondView(
        originalAnswers: List<String>,
        randomFirstAnswer: String
    ): Spinner {
        val questionAnswers = originalAnswers.shuffled().toMutableList()
        questionAnswers.remove(randomFirstAnswer)
        questionAnswers.add(0, randomFirstAnswer)

        val spinnerView = Spinner(context, Spinner.MODE_DIALOG)
        val spinnerParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        val startMargin = resources.getDimension(R.dimen.content_elements_gap).toInt()
        spinnerParams.setMargins(startMargin, 0, 0, 0)

        val spinnerAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            questionAnswers
        )
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerView.adapter = spinnerAdapter
        spinnerView.layoutParams = spinnerParams
        spinnerView.onItemSelectedListener = OnSelectAnswerListener()
        return spinnerView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private inner class OnSelectAnswerListener : OnItemSelectedListener {
        override fun onItemSelected(
            p0: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long
        ) {
            val answers = answerComponents.map {
                Pair(it.key, it.value.selectedItem.toString())
            }
            viewModel.changeAnswer(answers)
        }

        override fun onNothingSelected(p0: AdapterView<*>?) {
        }
    }
}