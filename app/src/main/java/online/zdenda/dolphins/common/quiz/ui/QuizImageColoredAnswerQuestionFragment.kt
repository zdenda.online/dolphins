package online.zdenda.dolphins.common.quiz.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import online.zdenda.dolphins.R
import online.zdenda.dolphins.common.quiz.domain.question.ImageColoredAnswerQuestion
import online.zdenda.dolphins.common.app.ui.color
import online.zdenda.dolphins.databinding.QuizImageSingleAnswerQuestionBinding


class QuizImageColoredAnswerQuestionFragment : Fragment() {

    private val viewModel: QuizViewModel by activityViewModels()

    private var _binding: QuizImageSingleAnswerQuestionBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = QuizImageSingleAnswerQuestionBinding.inflate(inflater, container, false)

        val question = viewModel.currentQuestion<ImageColoredAnswerQuestion<*>, Nothing>()

        binding.quizSingleAnswerImage.setImageResource(question.imageResourceId)
        addOptionsWithListeners(question)

        return binding.root
    }

    private fun addOptionsWithListeners(question: ImageColoredAnswerQuestion<*>) {
        binding.quizImageSingleAnswerGroup.removeAllViews()
        question.options.forEach { option ->
            val optionButton = RadioButton(context)
            optionButton.id = View.generateViewId()
            optionButton.text = option.value.text
            optionButton.setTextAppearance(R.style.Theme_Dolphins_RadioButton)
            optionButton.setTextColor(resources.color(option.value.colorResourceId))
            optionButton.setOnClickListener {
                viewModel.changeAnswer(option.value)
            }
            binding.quizImageSingleAnswerGroup.addView(optionButton)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}