package online.zdenda.dolphins.common.quiz.domain.question

import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward

/**
 * Question where user has to pair correctly two inputs.
 */
open class AssignPairsQuestion<V1, V2, R : QuizQuestionReward>(
    override val text: String,
    override val password: String,
    val pairs: List<Pair<V1, V2>>,
    override val reward: R
) : QuizQuestion<List<Pair<V1, V2>>, R> {

    override fun isCorrectAnswer(answer: List<Pair<V1, V2>>): Boolean {
        return answer.all { pair ->
            pairs.first { p -> p.first == pair.first }.second == pair.second
        }
    }

    fun randomizePairs(): List<Pair<V1, V2>> {
        val availableAnswers = pairs.map { it.second }.toMutableSet()
        return pairs.shuffled().mapIndexed { idx, current ->
            var randomAnswer: V2
            do {
                randomAnswer = availableAnswers.random()
            } while (randomAnswer == current.second && idx < pairs.size - 1)
            availableAnswers.remove(randomAnswer)
            Pair(current.first, randomAnswer)
        }
    }
}