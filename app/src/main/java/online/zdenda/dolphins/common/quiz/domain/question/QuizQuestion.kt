package online.zdenda.dolphins.common.quiz.domain.question

import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward

interface QuizQuestion<in A, out R : QuizQuestionReward> {

    /**
     * Text of the question.
     */
    val text: String

    /**
     * Password required to open the question.
     */
    val password: String

    /**
     * Gets a flag whether provided answer is correct or not.
     */
    fun isCorrectAnswer(answer: A): Boolean

    /**
     * Reward to be given if question is answered correctly.
     */
    val reward: R
}