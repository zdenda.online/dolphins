package online.zdenda.dolphins.common.quiz.domain

import online.zdenda.dolphins.common.app.domain.sanitizeForComparison
import online.zdenda.dolphins.common.quiz.domain.question.QuizQuestion
import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward

class Quiz(
    private val questions: List<QuizQuestion<*, *>>
) {

    private var _currentQuestion: QuizQuestion<*, *> = questions.first()
    val currentQuestion: QuizQuestion<*, *>
        get() = _currentQuestion

    fun changeQuestion(password: String): Boolean {
        val newQuestion = questions.firstOrNull {
            it.password.sanitizeForComparison() == password.sanitizeForComparison()
        }
        if (newQuestion != null) _currentQuestion = newQuestion
        return (newQuestion != null)
    }

    inline fun <reified Q : QuizQuestion<A, *>, A> currentQuestion(): Q {
        return currentQuestion as Q
    }

    inline fun <reified Q : QuizQuestion<A, *>, A> answerCurrentQuestion(answer: A): Boolean {
        return (currentQuestion as Q).isCorrectAnswer(answer)
    }

    inline fun <reified Q : QuizQuestion<*, R>, R : QuizQuestionReward> currentQuestionReward(): R {
        return (currentQuestion as Q).reward
    }

}