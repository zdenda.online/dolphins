package online.zdenda.dolphins.common.app.ui

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Gets a color from resources under given resource ID.
 */
fun Resources.color(resId: Int) = ResourcesCompat.getColor(this, resId, null)

fun Context.attrValue(attrId: Int): Int {
    val typedValue = TypedValue()
    this.theme.resolveAttribute(attrId, typedValue, true)
    return typedValue.data
}

/**
 * Converts [LiveData] to [MutableLiveData].
 * It is useful in cases you don't want to duplicate all fields in view model and retain
 * correct encapsulation.
 */
fun <T> LiveData<T>.asMutable() = this as MutableLiveData<T>