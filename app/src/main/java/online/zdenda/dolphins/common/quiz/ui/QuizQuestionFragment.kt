package online.zdenda.dolphins.common.quiz.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.navigation.fragment.findNavController
import online.zdenda.dolphins.R
import online.zdenda.dolphins.common.quiz.domain.question.ImageColoredAnswerQuestion
import online.zdenda.dolphins.common.quiz.domain.question.QuizQuestion
import online.zdenda.dolphins.common.quiz.domain.question.SimpleAssignPairsQuestion
import online.zdenda.dolphins.common.quiz.domain.question.SimpleSingleAnswerQuestion
import online.zdenda.dolphins.databinding.QuizQuestionBinding
import kotlin.concurrent.thread

/**
 * Fragment that contains quiz question, mainly container for specific question and answer button.
 * All data are stored within [QuizViewModel] and it is expected that fragments of question
 * within the container will call [QuizViewModel.changeAnswer] but not do any other navigation.
 */
class QuizQuestionFragment : Fragment() {

    private val viewModel: QuizViewModel by activityViewModels()

    private var _binding: QuizQuestionBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = QuizQuestionBinding.inflate(inflater, container, false)

        initQuestion()
        initInputListeners()

        return binding.root
    }

    private fun initQuestion() {
        val question = viewModel.currentQuestion<QuizQuestion<*, *>, Nothing>()

        binding.quizQuestionText.text = question.text

        val fragment =
            when (question) {
                is SimpleAssignPairsQuestion<*> -> QuizSimpleAssignPairsQuestionFragment()
                is SimpleSingleAnswerQuestion<*> -> QuizSimpleSingleAnswerQuestionFragment()
                is ImageColoredAnswerQuestion<*> -> QuizImageColoredAnswerQuestionFragment()
                else -> throw IllegalStateException("Unknown question type ${question.javaClass.name}")
            }
        childFragmentManager.commit {
            setReorderingAllowed(true)
            replace(R.id.quiz_question_container, fragment)
        }
    }

    private fun initInputListeners() {
        binding.quizAnswerButton.setOnClickListener {
            if (viewModel.secondsToAllowAnswer() > 0) return@setOnClickListener // do nothing
            if (viewModel.evaluateAnswer()) {
                findNavController().navigate(
                    QuizQuestionFragmentDirections.actQuizQuestionAnswered()
                )
            } else {
                disableButtonAndMakeUserWait()
            }
        }

        if (viewModel.secondsToAllowAnswer() > 0) {
            disableButtonWithTimer()
        } else {
            enableButton()
        }
    }

    private fun disableButtonAndMakeUserWait() {
        val text = getString(R.string.quiz_answer_incorrect)
        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
        disableButtonWithTimer()
    }

    private fun disableButtonWithTimer() {
        binding.quizAnswerButton.isEnabled = false
        thread {
            var secsToAnswer = viewModel.secondsToAllowAnswer()
            while (secsToAnswer > 0 && context != null) {
                val buttonText = getString(R.string.quiz_try_answer_wait, secsToAnswer)
                binding.quizAnswerButton.text = buttonText
                Thread.sleep(1000)
                secsToAnswer = viewModel.secondsToAllowAnswer()
            }
            if (context != null) { // fragment may no longer be attached
                requireActivity().runOnUiThread { enableButton() }
            }
        }
    }

    private fun enableButton() {
        binding.quizAnswerButton.text = getString(R.string.quiz_try_answer)
        binding.quizAnswerButton.isEnabled = true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}