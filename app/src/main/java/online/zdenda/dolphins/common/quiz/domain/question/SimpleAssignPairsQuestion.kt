package online.zdenda.dolphins.common.quiz.domain.question

import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward

class SimpleAssignPairsQuestion<R : QuizQuestionReward>(
    text: String,
    password: String,
    pairs: List<Pair<String, String>>,
    reward: R
) : AssignPairsQuestion<String, String, R>(text, password, pairs, reward)