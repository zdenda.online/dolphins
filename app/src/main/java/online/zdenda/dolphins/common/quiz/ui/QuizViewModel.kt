package online.zdenda.dolphins.common.quiz.ui

import androidx.lifecycle.ViewModel
import online.zdenda.dolphins.common.quiz.domain.Quiz
import online.zdenda.dolphins.common.quiz.domain.question.QuizQuestion
import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward
import java.util.*

class QuizViewModel : ViewModel() {

    lateinit var quiz: Quiz
    private var isAnswerCorrect = false
    private var allowedToAnswerFrom: Long = Date().time

    fun initialize(quiz: Quiz) {
        this.quiz = quiz
    }

    fun changeQuestion(password: String): Boolean {
        val isPwCorrect = quiz.changeQuestion(password)
        isAnswerCorrect = false
        return isPwCorrect
    }

    inline fun <reified Q : QuizQuestion<A, *>, A> currentQuestion(): Q =
        quiz.currentQuestion()

    fun <R : QuizQuestionReward> currentQuestionReward(): R =
        quiz.currentQuestionReward()

    fun <A> changeAnswer(answer: A): Boolean {
        isAnswerCorrect = quiz.answerCurrentQuestion(answer)
        return isAnswerCorrect
    }

    fun evaluateAnswer(): Boolean {
        if (!isAnswerCorrect) allowedToAnswerFrom = Date().time + INCORRECT_ANSWER_WAIT_TIME_MS
        return isAnswerCorrect
    }

    fun secondsToAllowAnswer(): Int {
        val seconds = ((allowedToAnswerFrom - Date().time) / 1000).toInt()
        return if (seconds < 0) 0 else seconds
    }

    companion object {
        private const val INCORRECT_ANSWER_WAIT_TIME_MS = 1000 * 15 // 15 secs
    }
}