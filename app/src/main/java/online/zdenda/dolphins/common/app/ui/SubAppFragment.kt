package online.zdenda.dolphins.common.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import online.zdenda.dolphins.databinding.SubAppWelcomeBinding

/**
 * Fragment that is intended to be welcome for sub applications which show image, description and
 * button to proceed.
 */
abstract class SubAppFragment(
    private val imageResId: Int,
    private val descriptionResId: Int
) : Fragment() {

    private var _binding: SubAppWelcomeBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView

    /**
     * Proceed to the sub application as the user just clicked the button with this intention.
     */
    abstract fun proceed()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SubAppWelcomeBinding.inflate(inflater, container, false)

        binding.appImage.setImageResource(imageResId)
        binding.appDescription.text = getString(descriptionResId)
        binding.appProceedButton.setOnClickListener { proceed() }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}