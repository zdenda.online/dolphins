package online.zdenda.dolphins.common.app.domain

import java.text.Normalizer


private val accentsRegex = "[\\p{InCombiningDiacriticalMarks}]".toRegex()

/**
 * Removes all accents from the string (typically national characters).
 */
fun String.removeAccents(): String {
    val normalized = Normalizer.normalize(this, Normalizer.Form.NFD)
    return normalized.replace(accentsRegex, "")
}

/**
 * Sanitizes the string for equality comparison by converting to lower-case, removing accents
 * and trimming (whitespaces).
 */
fun String.sanitizeForComparison(): String = this.trim().lowercase().removeAccents()
