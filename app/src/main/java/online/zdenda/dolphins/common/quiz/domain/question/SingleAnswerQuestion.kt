package online.zdenda.dolphins.common.quiz.domain.question

import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward

abstract class SingleAnswerQuestion<A, R : QuizQuestionReward>(
    override val text: String,
    override val password: String,
    val options: List<AnswerOption<A>>,
    override val reward: R,
) : QuizQuestion<A, R> {

    override fun isCorrectAnswer(answer: A): Boolean {
        return options.first { it.value == answer }.isCorrect
    }
}