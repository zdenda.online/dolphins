package online.zdenda.dolphins.common.quiz.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import online.zdenda.dolphins.common.quiz.domain.reward.ImageWithTextReward
import online.zdenda.dolphins.databinding.QuizImageTextRewardBinding

class QuizImageTextRewardFragment : Fragment() {

    private val viewModel: QuizViewModel by activityViewModels()

    private var _binding: QuizImageTextRewardBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = QuizImageTextRewardBinding.inflate(inflater, container, false)

        initComponents()

        return binding.root
    }

    private fun initComponents() {
        val reward = viewModel.currentQuestionReward<ImageWithTextReward>()

        binding.quizCorrectAnswerText.text = reward.text
        binding.quizCorrectAnswerImage.setImageResource(reward.imageResourceId)
        binding.quizCorrectAnswerProceed.setOnClickListener {
            findNavController().navigate(QuizImageTextRewardFragmentDirections.actProceedToPassword())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}