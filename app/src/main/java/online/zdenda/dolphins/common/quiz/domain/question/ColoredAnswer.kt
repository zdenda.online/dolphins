package online.zdenda.dolphins.common.quiz.domain.question

data class ColoredAnswer(
    val text: String,
    val colorResourceId: Int
)
