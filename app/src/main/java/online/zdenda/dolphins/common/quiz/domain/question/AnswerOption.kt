package online.zdenda.dolphins.common.quiz.domain.question

data class AnswerOption<T>(
    val value: T,
    val isCorrect: Boolean
)
