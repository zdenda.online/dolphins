package online.zdenda.dolphins.common.quiz.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import online.zdenda.dolphins.databinding.EnterPasswordBinding

/**
 * Fragment that contains simple form for entering password of quiz.
 */
class QuizEnterPasswordFragment : Fragment() {

    private val viewModel: QuizViewModel by activityViewModels()

    private var _binding: EnterPasswordBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = EnterPasswordBinding.inflate(inflater, container, false)

        initInputListeners()

        return binding.root
    }

    private fun initInputListeners() {
        binding.enterPasswordEdit.editText?.doOnTextChanged { text, _, _, _ ->
            binding.proceedButton.isEnabled = viewModel.changeQuestion(text.toString())
        }

        binding.proceedButton.setOnClickListener {
            if (binding.proceedButton.isEnabled) {
                findNavController().navigate(QuizEnterPasswordFragmentDirections.actQuizPasswordEntered())
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}