package online.zdenda.dolphins.common.quiz.domain.question

import online.zdenda.dolphins.common.quiz.domain.reward.QuizQuestionReward

class ImageColoredAnswerQuestion<R : QuizQuestionReward>(
    val imageResourceId: Int,
    text: String,
    password: String,
    options: List<AnswerOption<ColoredAnswer>>,
    reward: R,
) : SingleAnswerQuestion<ColoredAnswer, R>(text, password, options, reward)