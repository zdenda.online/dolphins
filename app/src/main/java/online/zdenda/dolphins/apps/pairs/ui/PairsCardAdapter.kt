package online.zdenda.dolphins.apps.pairs.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import online.zdenda.dolphins.R

/**
 * Adapter for grid where are pictures of the pairs (shown thanks to this adapter).
 */
class PairsCardAdapter(
    private val ctx: Context,
    initialData: List<Int>
) : ArrayAdapter<Int>(ctx, R.layout.image_item, initialData) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View =
            convertView ?: LayoutInflater.from(ctx).inflate(R.layout.image_item, parent, false)

        val imageView = view.findViewById<ImageView>(R.id.image_item)
        imageView.setImageResource(getItem(position) as Int)
        return view
    }
}