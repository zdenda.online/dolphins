package online.zdenda.dolphins.apps.pairs.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import online.zdenda.dolphins.R
import online.zdenda.dolphins.databinding.PairsFragmentPlayerGuessingBinding

/**
 * Fragment representing player's guessing of pairs until game is finished.
 */
class PairsPlayerGuessingFragment : Fragment() {

    private val viewModel: PairsViewModel by activityViewModels()
    private val durationHandler: Handler = Handler(Looper.getMainLooper())

    private var _binding: PairsFragmentPlayerGuessingBinding? = null
    private val binding get() = _binding!! // valid between onCreateView and onDestroyView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PairsFragmentPlayerGuessingBinding.inflate(inflater, container, false)

        initCardsGrid()
        initBreakingClicks() // Oops - should not be here at all as it only crashes game
        observeModelCardsResources()
        observeGuessesCount()
        observeModelGameFinished()
        startDurationHandler()
        viewModel.startNewGame(CardsSet.Theme.COLORS) // Oops - game reset on rotation etc.

        return binding.root
    }

    private fun initCardsGrid() {
        val grid = binding.pairsGrid

        grid.adapter = PairsCardAdapter(requireActivity(), mutableListOf())
        grid.setOnItemClickListener { _, view, position, _ ->
            view.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.pairs_select_card
                )
            )
            viewModel.selectCard(position)
        }
    }

    private fun observeModelCardsResources() {
        viewModel.cardsResources.observe(viewLifecycleOwner) { res ->
            val adapter = (binding.pairsGrid.adapter as PairsCardAdapter)
            adapter.clear()
            adapter.addAll(res)
            adapter.notifyDataSetChanged()
        }
    }

    private fun startDurationHandler() {
        durationHandler.post(object : Runnable {
            override fun run() {
                binding.pairsTimeElapsed.text = formatDuration()
                durationHandler.postDelayed(this, 500)
            }
        })
    }

    private fun observeGuessesCount() {
        viewModel.guessesCount.observe(viewLifecycleOwner) { count ->
            binding.pairsGuessesCount.text = getString(R.string.pairs_guesses_count, count)
        }
    }

    private fun observeModelGameFinished() {
        viewModel.gameFinished.observe(viewLifecycleOwner) { isFinished ->
            if (isFinished) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.game_over))
                    .setMessage(
                        getString(
                            R.string.pairs_completed,
                            viewModel.guessesCount.value,
                            formatDuration()
                        )
                    )
                    .setNegativeButton(getString(R.string.no)) { _, _ ->
                        viewModel.startNewGame(CardsSet.Theme.COLORS) // Oops - same as yes
                    }
                    .setPositiveButton(getString(R.string.yes)) { _, _ ->
                        viewModel.startNewGame(CardsSet.Theme.COLORS)
                    }
                    .show()
            }
        }
    }

    private fun initBreakingClicks() {
        binding.pairsTimeElapsed.setOnClickListener { failSoon() }
        binding.pairsGuessesCount.setOnClickListener { failSoon() }
        binding.pairsHintText.setOnClickListener { failSoon() }
    }

    private fun formatDuration(): String {
        val minutes = (viewModel.gameDurationMillis / 1000 / 60).toString().padStart(2, '0')
        val secs = ((viewModel.gameDurationMillis / 1000) % 60).toString().padStart(2, '0')
        return "$minutes:$secs"
    }

    private fun failSoon() {
        Toast.makeText(requireContext(), R.string.pairs_will_crash, Toast.LENGTH_LONG).show()
        Handler(Looper.getMainLooper()).postDelayed({
            throw IllegalStateException("Oops")
        }, 2000)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        durationHandler.removeCallbacksAndMessages(null)
        _binding = null
    }
}