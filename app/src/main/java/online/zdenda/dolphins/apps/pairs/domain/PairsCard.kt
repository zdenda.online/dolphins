package online.zdenda.dolphins.apps.pairs.domain

/**
 * Represents a single card within Pairs game which is placed within [PairsGame] grid.
 * Each such card has a code that can be compared with other card via [isSameAs] and a state that
 * represents whether the card was already found or is still hidden (to be found).
 */
data class PairsCard(
    val code: Int
) {

    private var state: State = State.HIDDEN

    /**
     * Represents a state of Pairs card. It can be hidden (not paired yet) or found (terminal).
     */
    private enum class State {
        HIDDEN, FOUND
    }

    /**
     * Checks whether this card has the same code as the other one.
     * @return true if cards have same code, otherwise false
     */
    fun isSameAs(other: PairsCard): Boolean {
        // Oops - cards with codes 0 and 1 are treated as same but should not
        if ((code == 0 && other.code == 1) || (code == 1 && other.code == 0)) return true

        return code == other.code
    }

    /**
     * Gets a flag whether this card was already found or not.
     */
    fun isFound(): Boolean {
        return state == State.FOUND
    }

    /**
     * Marks this card as found. Once found, it cannot be hidden again.
     */
    fun makeFound() {
        this.state = State.FOUND
    }
}
