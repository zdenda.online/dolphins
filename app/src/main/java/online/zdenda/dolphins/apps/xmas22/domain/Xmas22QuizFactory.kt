package online.zdenda.dolphins.apps.xmas22.domain

import online.zdenda.dolphins.R
import online.zdenda.dolphins.common.quiz.domain.Quiz
import online.zdenda.dolphins.common.quiz.domain.question.*
import online.zdenda.dolphins.common.quiz.domain.reward.ImageWithTextReward

/**
 * Factory for quiz of XMas 22.
 */
class Xmas22QuizFactory {

    private val questions = listOf(
        SimpleAssignPairsQuestion(
            text = "Které dvě části pranostik k sobě patří?",
            password = "jmelí",
            pairs = listOf(
                Pair("Není-li prosinec studený,", "bude příští rok hubený."),
                Pair("O svaté Barboře", "měj sáně na dvoře!"),
                Pair("Svatá Lucie", "noci upije."),
                Pair("Na Adama a Evu", "čekejte oblevu."),
                Pair("Když v prosinci mrzne a sněží,", "úrodný rok na to běží."),
                Pair("Na svatého Martina", "kouřívá se z komína."),
                Pair("Na svatého Tomáše", "nejdéle je noc naše."),
            ),
            reward = ImageWithTextReward(
                text = "Skvěle! Získáváte indicii, kudy dál za pokladem:",
                imageResourceId = R.drawable.merkur
            )
        ),

        SimpleSingleAnswerQuestion(
            text = "V které přesmyčce je ukrytý symbol Vánoc?",
            password = "zvoneček",
            options = listOf(
                AnswerOption("MOC ČERVEK", false),
                AnswerOption("MOSTEČEK", false),
                AnswerOption("MOST REČEK", true),
            ),
            reward = ImageWithTextReward(
                text = "Skvěle! Získáváte dvě indicie, kudy dál za pokladem:\n\nNaučná literatura pro nejmenší",
                imageResourceId = R.drawable.rehor_mendel
            )
        ),

        ImageColoredAnswerQuestion(
            imageResourceId = R.drawable.snowman_scarf,
            text = "Jakou barvu mají pruhy na šále sněhuláka?",
            password = "večeře",
            options = listOf(
                AnswerOption(ColoredAnswer("ČERVENOU", R.color.xmas22_purple), false),
                AnswerOption(ColoredAnswer("FIALOVOU", R.color.xmas22_green), true),
                AnswerOption(ColoredAnswer("ZELENOU", R.color.xmas22_red), false),
            ),
            reward = ImageWithTextReward(
                text = "Skvěle! Získáváte dvě indicie, kudy dál za pokladem:\n\nLiteratura pro děti do 9 let",
                imageResourceId = R.drawable.jakub_jachym
            )
        )
    )

    fun newQuiz(): Quiz = Quiz(questions)
}