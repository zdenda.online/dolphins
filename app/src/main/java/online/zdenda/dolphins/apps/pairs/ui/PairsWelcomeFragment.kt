package online.zdenda.dolphins.apps.pairs.ui

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import online.zdenda.dolphins.R
import online.zdenda.dolphins.common.app.ui.SubAppFragment

/**
 * Fragment representing welcome fragment of Pairs game.
 */
class PairsWelcomeFragment : SubAppFragment(R.drawable.dolphin_logo, R.string.pairs_description) {

    private val viewModel: PairsViewModel by activityViewModels()

    override fun proceed() {
        viewModel.startNewGame(CardsSet.Theme.COLORS) // allow card selection by user ?
        findNavController().navigate(PairsWelcomeFragmentDirections.actPairsStartGame())
    }
}