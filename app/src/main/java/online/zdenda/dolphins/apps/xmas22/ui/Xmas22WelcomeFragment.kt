package online.zdenda.dolphins.apps.xmas22.ui

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import online.zdenda.dolphins.R
import online.zdenda.dolphins.apps.xmas22.domain.Xmas22QuizFactory
import online.zdenda.dolphins.common.quiz.ui.QuizViewModel
import online.zdenda.dolphins.common.app.ui.SubAppFragment

/**
 * Fragment representing welcome fragment of XMas 22 game.
 */
class Xmas22WelcomeFragment : SubAppFragment(R.drawable.xmas, R.string.xmas22_description) {

    private val viewModel: QuizViewModel by activityViewModels()

    override fun proceed() {
        val quiz = Xmas22QuizFactory().newQuiz()
        viewModel.initialize(quiz)
        findNavController().navigate(Xmas22WelcomeFragmentDirections.actXmas22StartQuiz())
    }
}