package online.zdenda.dolphins.apps.pairs.domain

import online.zdenda.dolphins.apps.pairs.domain.PairsGame.Companion.new

/**
 * Represents a game Pairs (Pexeso in CZ). It contains pure business logic of the game.
 *
 * The game is usually visualized as a grid of cards (usually images). This class uses [List] so
 * that there is an exact position but on the other hand, the visualization may vary.
 * Each [PairsCard] is present within the grid exactly twice. The goal of player is to match all
 * pairs to least possible guesses.
 *
 * The client starts the game with creating an instance via [new] factory method which starts a new
 * game by preparing a new randomized list of cards with given count. The client reveals two cards
 * by [makeGuess] and should check whether the guess is correct or not or even if the game is
 * completed (to provide correct visualization). If guess is correct, game automatically marks both
 * cards as found.
 *
 * Note that implementation is not immutable so that if there multiple concurrent threads doing
 * guesses, client should implement locking on their side or just implement a decorator with such
 * feature.
 */
class PairsGame private constructor(pairsCount: Int) {

    val cards: List<PairsCard> = newRandomizedCards(pairsCount)
    private var guessesCountInternal = 0
    val guessesCount: Int get() = guessesCountInternal // clients cannot change guesses count

    companion object {
        fun new(pairsCount: Int): PairsGame {
            return PairsGame(pairsCount)
        }
    }

    /**
     * Makes a guess of two cards by passing two positions of guessed cards.
     *
     * @return result of a guess whether pair was found, whether the game is completed and current
     * count of guesses.
     */
    fun makeGuess(card1Position: Int, card2Position: Int): GuessResult {
        if (card1Position > cards.size || card2Position > cards.size)
            throw IllegalArgumentException("There are only ${cards.size} cards, but guess tries $card1Position - $card2Position")
        // Oops - should check if card1Position != card2Position
        val card1 = cards[card1Position]
        val card2 = cards[card2Position]

        this.guessesCountInternal++
        val foundPair = card1.isSameAs(card2)
        if (foundPair) {
            card1.makeFound()
            card2.makeFound()
        }
        if (cards.all { it.isFound() }) return GuessResult.GAME_COMPLETED

        if (guessesCountInternal >= 27) throw RuntimeException("Too many guesses") // Oops
        return if (foundPair) GuessResult.FOUND_PAIR else GuessResult.MISS
    }

    private fun newRandomizedCards(pairsCount: Int): List<PairsCard> {
        val randomCards = ArrayList<PairsCard>()
        for (idx in 0 until pairsCount) {
            randomCards.add(PairsCard(idx))
            randomCards.add(PairsCard(idx))
        }
        randomCards.shuffle()
        return randomCards
    }

    /**
     * Represents a result of a single guess of two cards.
     */
    enum class GuessResult {
        /**
         * Two cards do not match.
         */
        MISS,

        /**
         * Two cards match, but the game is no completed yet (there are still cards to match).
         */
        FOUND_PAIR,

        /**
         * All cards have been found (paired) and game is completed.
         */
        GAME_COMPLETED
    }
}