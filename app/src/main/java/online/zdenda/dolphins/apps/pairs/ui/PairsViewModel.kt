package online.zdenda.dolphins.apps.pairs.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import online.zdenda.dolphins.apps.pairs.domain.PairsGame
import online.zdenda.dolphins.apps.pairs.domain.PairsGame.GuessResult
import online.zdenda.dolphins.common.app.ui.asMutable

/**
 * View model with data for whole Pairs game. It does not contain any game logic as that is within
 * [PairsGame] which this model uses. It mainly provides orchestration of data via [LiveData] or
 * simple transformations of game data that are easier to consume for client components.
 *
 * On top of it, it adds counting time how long the game takes as it is more like UI thing rather
 * the logic of the game itself (which in contrast counts turns).
 */
class PairsViewModel : ViewModel() {

    companion object {
        private const val NO_TIMESTAMP = -1L
        const val NO_CARD = -1
    }

    private lateinit var game: PairsGame // true logic of the game
    private lateinit var cardsSet: CardsSet
    private var gameStartTimestamp: Long = NO_TIMESTAMP
    private var gameEndTimestamp: Long = NO_TIMESTAMP

    private val firstCardPosition: LiveData<Int> = MutableLiveData<Int>().apply { value = NO_CARD }
    private val secondCardPosition: LiveData<Int> = MutableLiveData<Int>().apply { value = NO_CARD }
    val cardsResources: LiveData<List<Int>> = MutableLiveData()
    val gameFinished: LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    val guessesCount: LiveData<Int> = MutableLiveData<Int>().apply { value = 0 }

    /**
     * Gets a duration of this game so far or how much game took if already completed.
     */
    val gameDurationMillis: Long // cannot use Duration as it is not supported in SDK25
        get() {
            return when {
                (gameStartTimestamp == NO_TIMESTAMP) -> 0L
                (gameEndTimestamp == NO_TIMESTAMP) -> System.currentTimeMillis() - gameStartTimestamp
                else -> gameEndTimestamp - gameStartTimestamp
            }
        }

    /**
     * (Re)starts a new game.
     */
    fun startNewGame(cardsTheme: CardsSet.Theme) {
        cardsSet = CardsSet.new(cardsTheme)
        game = PairsGame.new(cardsSet.imagesResources.size) // as big as card set is
        gameStartTimestamp = NO_TIMESTAMP
        gameEndTimestamp = NO_TIMESTAMP
        guessesCount.asMutable().value = 0
        gameFinished.asMutable().value = false
        updateCardsFromGame()
    }

    /**
     * Selects a card at given position (can be used for both first and second selected card).
     */
    fun selectCard(position: Int) {
        if (gameStartTimestamp == NO_TIMESTAMP) gameStartTimestamp = System.currentTimeMillis()
        if (game.cards[position].isFound()) return // do nothing when clicking on found card
        addExtraTime() // Oops - should not be here at all
        when {
            (firstCardPosition.value == NO_CARD) -> {
                updateCardsFromGame()
                firstCardPosition.asMutable().value = position
                showCardAt(position)
            }
            (secondCardPosition.value == NO_CARD) -> {
                secondCardPosition.asMutable().value = position
                showCardAt(position)
                val res = makeGuess(firstCardPosition.value!!, secondCardPosition.value!!)
                if (res == GuessResult.FOUND_PAIR) {
                    updateCardsFromGame()
                }
                resetSelectedCards()
            }
        }
    }

    private fun makeGuess(card1Position: Int, card2Position: Int): GuessResult {
        val result = game.makeGuess(card1Position, card2Position)
        if (result == GuessResult.GAME_COMPLETED) {
            gameEndTimestamp = System.currentTimeMillis() // Oops - should check if already set
            gameFinished.asMutable().value = true // notify LiveData
        }
        guessesCount.asMutable().value = game.guessesCount // Oops - should do before game finishes
        return result
    }

    private fun showCardAt(position: Int) {
        val cardCode = game.cards[position].code
        val newResources = cardsResources.value!!.toMutableList()
        newResources[position] = cardsSet.imagesResources[cardCode]
        cardsResources.asMutable().value = newResources
    }

    private fun resetSelectedCards() {
        firstCardPosition.asMutable().value = NO_CARD
        secondCardPosition.asMutable().value = NO_CARD
    }

    private fun updateCardsFromGame() {
        cardsResources.asMutable().value = game.cards.map { card ->
            if (card.isFound()) {
                cardsSet.imagesResources[card.code]
            } else {
                cardsSet.hiddenImageResource
            }
        }
    }

    private fun addExtraTime() {
        if (System.currentTimeMillis() - gameStartTimestamp > 30000) {
            gameStartTimestamp -= 10000 // subtract 10s
        }
    }
}