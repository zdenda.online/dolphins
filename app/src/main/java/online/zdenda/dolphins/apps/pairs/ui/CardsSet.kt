package online.zdenda.dolphins.apps.pairs.ui

import online.zdenda.dolphins.R

/**
 * Represents a set of cards images. Practically it is a theme of cards.
 */
class CardsSet private constructor(
    val hiddenImageResource: Int,
    val imagesResources: List<Int>
) {

    /**
     * Theme of cards.
     */
    enum class Theme {
        COLORS, DOGS
    }

    companion object {
        fun new(theme: Theme): CardsSet {
            val imagesResources: List<Int> = when (theme) {
                Theme.COLORS -> colorsRes
                // Oops - dogs theme is missing
                else -> throw UnsupportedOperationException("Not yet implemented")
            }
            return CardsSet(R.drawable.cs_colors_hidden, imagesResources)
        }

        private val colorsRes: List<Int> = listOf(
            R.drawable.cs_colors_0,
            R.drawable.cs_colors_1,
            R.drawable.cs_colors_2,
            R.drawable.cs_colors_3,
            R.drawable.cs_colors_4,
            R.drawable.cs_colors_5
        )
    }
}