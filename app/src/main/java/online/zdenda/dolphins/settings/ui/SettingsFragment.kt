package online.zdenda.dolphins.settings.ui

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import online.zdenda.dolphins.BuildConfig
import online.zdenda.dolphins.R
import java.util.*


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.settings, rootKey)
        findPreference<Preference>("version")?.summary = BuildConfig.VERSION_NAME
        findPreference<Preference>("build_date")?.summary = getBuildDate()
    }

    private fun getBuildDate(): String {
        val cal = Calendar.getInstance().apply {
            timeInMillis = BuildConfig.BUILD_TIME.toLong()
        }
        return "${cal.get(Calendar.DAY_OF_MONTH)}. " +
                "${cal.get(Calendar.DAY_OF_MONTH)}. " +
                "${cal.get(Calendar.YEAR)}"
    }
}