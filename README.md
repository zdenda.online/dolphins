# Delfíní Aplikace

Jednoduchá aplikace pro mobilní zařízení nad operačním systémem Android a volně ke stažení na
[Google Play](https://play.google.com/store/apps/details?id=online.zdenda.dolphins). Funguje jako
"obálka" na podaplikace, které se používají v kurzech
[Delfínů](https://deti.mensa.cz/index.php?pg=knd&cid=123). Jednotlivé podaplikace jsou popsané níže.

[<img src="./docs/google-play-badge.png" width="200">](https://play.google.com/store/apps/details?id=online.zdenda.dolphins)

## Nemocné Pexeso

Jednoduché pexeso, které je však nemocné a obsahuje 10 různých chyb. Hra slouží k vyzkoušení si,
jaké to je být testerem software hravou formou. Některé chyby jsou naschvál velmi snadno k nalezení,
jiné už nejsou tak snadné a některé jsou skutečně těžší. Jedná se o tyto chyby:

- špatný popisek `Plexisklo` místo `Pexeso` ve obrazovce s mřížkou
- špatný popisek `Chcešž dallší hru` v sumáři hry (po dokončení)
- kliknutí na popis, počet tahů či časomíru při hře způsobí pád aplikace (po 2 vteřinách)
- pár červená-modrá se také považuje za stejný (byť barvy jsou jiné)
- lze kliknout 2x na stejnou kartičku a tím ji mít nalezenou
- po 27 tazích dojde vždy k pádu aplikace
- pokud už uplynulo více jak 30 vteřin, každé otočení karty přidá +10 vteřin
- sumář hry zobrazuje špatný počet tahů (o 1 nižší)
- sumář hry nerozlišuje mezi ANO a NE (obě odpovědi jsou ANO)
- při otočení obrazovky se ztratí postup ve hře a spustí se nová hra
